<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Web Demo</title>

  <link rel="stylesheet" href="newcss/style.css">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  <!-- fontawesome link -->
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

</head>

<body>
  <?php include('header.php') ?>

  <!-- navbar end -->

  <!-- top section -->
  <section>
    <div class="container mt-4">
      <div class="row">
        <div class="col-md-4">
          <div class="card">
            <div class="card-body">
              <h4 class="" style="font-weight: 600;">
                Zoyo Infosoft
              </h4>
              <p class="" style="font-weight: 400;">
                Krishna Plaza,1st floor<br> office no.- 13 & 14<br> Near SRS Mall, Bijnor
              </p>
              <p class=""><span style="font-weight: 600;">Website:</span> <span>www:https://zoyoecommerce.com</span></p>
              <p class=""><span style="font-weight: 600;">Email:</span> <span>info@zoyoecommerce.com</span></p>
              <p class=""><span style="font-weight: 600;">Phone:</span> <span>+91 7617786491</span></p>
            </div>
          </div>
        </div>
        <div class="col-md-8">
          <form action="#" method="GET" class="p-4 text-center" style="box-shadow: 0px 0px 5px rgb(194, 194, 194);">
            <div class="row">
              <div class="col mb-2">
                <label for="exampleInputname">Your Name</label>
                <input type="text" class="form-control" id="exampleInputname" aria-describedby="emailHelp" placeholder="Your name">
                <span id="nameerror"></span>
              </div>
              <div class="col mb-2">
                <label for="exampleInputEmail1">Your Email</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Your email">
                <span id="emailerror"></span>
              </div>
            </div>
            <div class="form-group mb-2">
              <label for="exampleInputsubject">Subject</label>
              <input type="text" class="form-control" id="exampleInputsubject" placeholder="Subject">
              <span id="suberror"></span>
            </div>
            <div class="form-group mb-2">
              <label for="exampleFormControlTextarea1">Your message</label>
              <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
            <button type="submit" class="btn submitbtn mt-3" onclick="myFunction()">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </section>


  <script>
    function myFunction() {
      let errormssg = document.getElementById('exampleInputname').value;


      if (errormssg == "") {
        document.getElementById('nameerror').innerHTML = "Name Filled Required!"
      } else {
        document.getElementById('nameerror').innerHTML = ""
      }
      let errormssg1 = document.getElementById('exampleInputEmail1').value;
      if (errormssg1 == "") {
        document.getElementById('emailerror').innerHTML = "Name Filled Required!"
      } else {
        document.getElementById('emailerror').innerHTML = ""
      }

    }
  </script>

  <!-- footer start -->

  <?php include('footer.php') ?>


  <!-- footer end -->





  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>