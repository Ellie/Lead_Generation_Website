<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Web Demo</title>

  <link rel="stylesheet" href="newcss/style.css">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  <!-- fontawesome link -->
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
    integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

</head>

<body>
  <?php include('header.php')?>

  <!-- navbar end -->

  <!-- top section -->
  <div class="container py-4">
    <div>
      <img src="image/3-SEO-strategie.png " class="img-fluid" alt="">
    </div>
  </div>
  <section>
    <div class="col-md-12 text-center ecommerce-sec">
      <div class="section-heading">
        <h2>Why Digital Platform is Must for Your Business?
        </h2>
      </div>
      <div>
        <hr>
      </div>
    </div>
    </div>
  </section>
  <section>
    <div class="container text-center">
      <div class="row">
        <div class="col-md-6">
          <div class="card text-white bg-info mb-3">
            <div class="card-body">
              <h5 class="card-title">FIRST IMPRESSION
              </h5>
              <p class="card-text">With online presence you can gain trust & credibility. In present era, people search
                for online review on anything & everything. Having presence and positive review online can help you in
                establishing Credibility & trust</p>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <img src="image/infographic-1.png" alt="">
        </div>
      </div>
      <hr>
      <div class="row mt-4">
        <div class="col-md-6">
          <img src="image/infographic-2.png" alt="">
        </div>
        <div class="col-md-6">
          <div class="card text-white bg-secondary mb-3">
            <div class="card-body">
              <h5 class="card-title">BOOST YOUR BRAND IMAGE
              </h5>
              <p class="card-text">
                Having a Website is a sign of professionalism that is a must to be portrayed. It's a formal way of
                introducing your company to the world.
              </p>
            </div>
          </div>
        </div>
      </div>
      <hr>
      <div class="row mt-4">
        <div class="col-md-6">
          <div class="card text-white bg-info mb-3">
            <div class="card-body">
              <h5 class="card-title">GENERATE INTEREST IN YOUR PRODUCTS & SERVICES
              </h5>
              <p class="card-text">
                Get more customer by creating more intrest for your product or any services with offers and promotions.
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <img src="image/infographic-3.png" alt="">
        </div>
      </div>
      <hr>
      <div class="row mt-4">
        <div class="col-md-6">
          <img src="image/infographic-4.png" alt="">
        </div>
        <div class="col-md-6">
          <div class="card text-white bg-secondary mb-3">
            <div class="card-body">
              <h5 class="card-title">24x7x365 AVAILABILITY
              </h5>
              <p class="card-text">
                Through a website you can make complete information of your business available 24x7 without any extra
                cost. </p>
            </div>
          </div>
        </div>
      </div>
      <hr>
      <div class="row mt-4">
        <div class="col-md-6">
          <div class="card text-white bg-info mb-3">
            <div class="card-body">
              <h5 class="card-title">TARGEING NEW AUDIENCE
              </h5>
              <p class="card-text">
                Website helps in targeting new audience with ease. You don't have to put extra efforts or employ a
                professional to handle it.
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <img src="image/infographic-5.png" alt="">
        </div>
      </div>
      <hr>
      <div class="row mt-4">
        <div class="col-md-6">
          <img src="image/infographic-6.png" alt="">
        </div>
        <div class="col-md-6">
          <div class="card text-white bg-secondary mb-3">
            <div class="card-body">
              <h5 class="card-title">WORLDWIDE EXPOSURE

              </h5>
              <p class="card-text">
                Through website you can make complete information of your business available 24*7 without extra cost.
              </p>
            </div>
          </div>
        </div>
      </div>
      <hr>
      <div class="row mt-4">
        <div class="col-md-6">
          <div class="card text-white bg-info mb-3">
            <div class="card-body">
              <h5 class="card-title">DESIRED TG
              </h5>
              <p class="card-text">
                With SEO you can target the desired audience on your website and get the leads of your business. Offline
                medium does not provide you hi-end filtration option for targeting specific group of people.
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <img src="image/infographic-7.png" alt="">
        </div>
      </div>
      <hr>
      <div class="row mt-4">
        <div class="col-md-6">
          <img src="image/infographic-8.png" alt="">
        </div>
        <div class="col-md-6">
          <div class="card text-white bg-secondary mb-3">
            <div class="card-body">
              <h5 class="card-title">COST SAVING
              </h5>
              <p class="card-text">
                Offline Medium wastes lost of stationary cost which cna be saved here. Moreover, Website is majorly one
                time investment to provide information to the masses.
              </p>
            </div>
          </div>
        </div>
      </div>
      <hr>
      <div class="row mt-4">
        <div class="col-md-6">
          <div class="card text-white bg-info mb-3">
            <div class="card-body">
              <h5 class="card-title">IMPROVED CUSTOMER SERVICE
              </h5>
              <p class="card-text">
                With website you can be available to your customers all the time and they can drop their query,
                suggestion and problem anything.
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <img src="image/infographic-9.png" alt="">
        </div>
      </div>
      <hr>
      <div class="row mt-4">
        <div class="col-md-6">
          <img src="image/infographic-10.png" alt="">
        </div>
        <div class="col-md-6">
          <div class="card text-white bg-secondary mb-3">
            <div class="card-body">
              <h5 class="card-title">DESIRED IMAGE PROJECTION
              </h5>
              <p class="card-text">
                Website creates an aura or rather first impression of the company. You can impress a distant customer
                and can project a desired image that is necessary for the initial business.
              </p>
            </div>
          </div>
        </div>
      </div>
      <hr>
      <div class="row mt-4">
        <div class="col-md-6">
          <div class="card text-white bg-info mb-3">
            <div class="card-body">
              <h5 class="card-title">ONE POINT PLATFORM

              </h5>
              <p class="card-text">
                Website is one point platform to provide all the information of your business to your customers,
                vendors, investors and people who are interested to join you.
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <img src="image/infographic-11.png" alt="">
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="container text-center">
      <div class="row">
        <div class="col-md-12 text-center ecommerce-sec mt-3">
          <div class="section-heading">
            <h2>What is Digital Platform
            </h2>
          </div>
          <div>
            <hr>
          </div>
        </div>
      </div>

      <div class="row mt-4">
        <div class="col-md-6 mt-4">
          <img src="image/digital_platform01.jpg" class="img-fluid" alt="">
        </div>
        <div class="col-md-6 mt-4">
          <div class="card text-white bg-secondary mb-3">
            <div class="card-body">

              <p class="card-text " style="text-align: justify;">
                A digital platform stage alludes to the Software or hardware of a site taking into account the
                connection of its clients. For example, Facebook is a digital platform. A platform is a fitting and play
                plan of action that permits different participants(producers and purchasers) to associate with it,
                communicate with one another and make and trade esteem.
              </p>
            </div>
          </div>
        </div>
      </div>
      <hr>
    </div>
  </section>
  <section>
    <div class="container text-center">
      <div class="row">
        <div class="col-md-12 text-center ecommerce-sec mt-3">
          <div class="section-heading">
            <h2>What are the different types of digital platforms?
            </h2>
          </div>
          <div>
            <hr>
          </div>
        </div>
      </div>
      <div class="row mt-4">
        <div class="col-md-6 mt-4">
          <div class="card text-white bg-secondary mb-3">
            <div class="card-body">
              <p class="card-text " style="text-align: justify;">
                The stage economy is financial and social action encouraged by stages. Such stages are normally online
                matchmakers or technology structures. The 2 primary groups of digital marketing are on the online and
                offline Marketing. Online digital marketing Platform has 7 significant classifications :- SEO, SMO, PPC
                promoting, Email advertising, Mobile Marketing, Viral Marketing.
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-6 mt-4">
          <img src="image/digital_platform02.jpg " class="img-fluid" alt="">
        </div>
      </div>

      <hr>
    </div>
  </section>
  <section>
    <div class="container text-center">
      <div class="row">
        <div class="col-md-12 text-center ecommerce-sec mt-3">
          <div class="section-heading">
            <h2>Unique strategy for digital platform.

            </h2>
          </div>
          <div>
            <hr>
          </div>
        </div>
      </div>
      <div class="row mt-4">
        <div class="col-md-6 mt-4">
          <div class="card text-white bg-secondary mb-3">
            <div class="card-body">
              <p class="card-text " style="text-align: justify;">
              <h4>
                Improve your Digital promoting stages with our center point page
              </h4>
              Utilize our center point pages as a kind of perspective to get up-to-speed on all the primary computerized
              showcasing mathods.
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-6 mt-4">
          <img src="image/unique-strategy.jpg" class="img-fluid" alt="">
        </div>
      </div>

      <hr>
    </div>
  </section>




  <!-- footer start -->

  <?php include('footer.php')?>


  <!-- footer end -->





  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>
</body>

</html>