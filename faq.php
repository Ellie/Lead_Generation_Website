<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Web Demo</title>

  <link rel="stylesheet" href=" newcss/style.css">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  <!-- fontawesome link -->
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
    integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

</head>

<body>
  <?php include('header.php')?>

  <!-- navbar end -->

  <!-- top section -->
  <div class="container py-4">
    <div>
      <img src="image/3-SEO-strategie.png" alt="">
    </div>

  </div>
  <section>
    <div class="comtainer text-center">

  <div>
    <h3 class="  my-4" style="font-weight: 600; color: rgb(9, 177, 243);">We're here to Help
        World?
    </h3>
    <hr class="" style="height: 20%; width: 20%; ">
</div>
<div>
    <h4>
      Frequency Answer and Question
    </h4>
</div>


    </div>
  </section>
  <section class="container py-4">

    <div id="accordion">
      <div class="card ">
        <div class="card-header " id="headingOne">
          <h5 class="mb-0">
            <button class="btn btn-link " style="font-weight: 600; text-decoration: none;" data-toggle="collapse"
              data-target="#collapseOne" aria-expanded="" aria-controls="collapseOne">
              Q: What is Business Domain in eCommerce Plan?
            </button>
          </h5>
        </div>

        <div id="collapseOne" class="collapse show " aria-labelledby="headingOne" data-parent="#accordion">
          <div class="card-body">
            The business name of your business will be registered as .com or .in
            For example, www.yourbusinessname.com
          </div>
        </div>
      </div>
      <div class="card ">
        <div class="card-header" id="headingTwo">
          <h5 class="mb-0">
            <button class="btn btn-link collapsed " style="text-decoration: none;font-weight: 600;"
              data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
              Q: What is Hosting in eCommerce Plan?
            </button>
          </h5>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
          <div class="card-body">
            Hosting comes with 30 gb of storage and unlimited bandwidth. More than 10,000 products can be uploaded in 30
            gb of space then the hosting plan can be upgraded as needed.
          </div>
        </div>
      </div>
      <div class="card ">
        <div class="card-header" id="headingThree">
          <h5 class="mb-0">
            <button class="btn btn-link collapsed" style="text-decoration: none; font-weight: 600;"
              data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
              Q: What is E-Commerce /Multivendor Website in eCommerce Plan?
            </button>
          </h5>
        </div>
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
          <div class="card-body">
            As per your product, eCommerce Website will be developed Which will be total dynamic. It's also used as
            Single Vendor or Multi Vendor.
            In which Product, Cart, Checkout, Wish List, Compare, Payment Gateway Etc are included.
          </div>
        </div>
      </div>
      <div class="card ">
        <div class="card-header" id="headingFour">
          <h5 class="mb-0">
            <button class="btn btn-link collapsed" style="text-decoration: none; font-weight: 600;"
              data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
              Q: What is Android Application in e-Commerce Plan?
            </button>
          </h5>
        </div>
        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
          <div class="card-body">
            As per your business website, Your Android Application will be developed which will be host on Google Play
            Store. As per Google Playstore Policy, Playstore Account will be provided by Client.
          </div>
        </div>
      </div>
      <div class="card ">
        <div class="card-header" id="headingFive">
          <h5 class="mb-0">
            <button class="btn btn-link collapsed" style="text-decoration: none; font-weight: 600;"
              data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
              Q: What is IOS Application in e-Commerce Plan?
            </button>
          </h5>
        </div>
        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
          <div class="card-body">
            As per your business website, Your IOS Application will be developed which will be host on App Store. As per
            App Store Policy, App Store Account will be provided by Client.
          </div>
        </div>
      </div>
      <div class="card ">
        <div class="card-header" id="headingSix">
          <h5 class="mb-0">
            <button class="btn btn-link collapsed" style="text-decoration: none; font-weight: 600;"
              data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
              Q: How Payment Gateway works in eCommerce Plan?
            </button>
          </h5>
        </div>
        <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
          <div class="card-body">
            PayTM's Payment Gateway will integrated with eCommerce Website & Application. Whereby ALL Debit card, Credit
            card, Net banking, BHIM And the PayTm wallet will accept the payment through Website & Application.
          </div>
        </div>
      </div>
      <div class="card ">
        <div class="card-header" id="headingSeven">
          <h5 class="mb-0">
            <button class="btn btn-link collapsed" style="text-decoration: none; font-weight: 600;"
              data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
              Q: What is Shipping Integration in eCommerce Plan?
            </button>
          </h5>
        </div>
        <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
          <div class="card-body">
            Their is Cusomized Shipping Integration means Customer can add details of courier services in which they
            courier order.
          </div>
        </div>
      </div>
      <div class="card ">
        <div class="card-header" id="headingEight">
          <h5 class="mb-0">
            <button class="btn btn-link collapsed" style="text-decoration: none; font-weight: 600;"
              data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseThree">
              Q: What is SMS Integration in eCommerce Plan?
            </button>
          </h5>
        </div>
        <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
          <div class="card-body">
            The functionality of SMS integration with the website, Android app and iOS app will also be integrated. At
            the time of User Registration, At Order Placed, At the time of order status change, Customer / Admin will
            get notification through SMS.
          </div>
        </div>
      </div>
      <div class="card ">
        <div class="card-header" id="headingNine">
          <h5 class="mb-0">
            <button class="btn btn-link collapsed" style="text-decoration: none; font-weight: 600;"
              data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
              Q: What is E-mail Integration in eCommerce Plan?
            </button>
          </h5>
        </div>
        <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
          <div class="card-body">
            The functionality of Email integration with the website, Android app and iOS app will also be integrated. At
            the time of User Registration, At Order Placed, At the time of order status change, Customer / Admin will
            get notification through Email.
          </div>
        </div>
      </div>

    </div>
  </section>
  <!-- top section end -->
  <!-- section -->


  <!-- footer start -->

  <?php include('footer.php')?>


  <!-- footer end -->





  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>
</body>

</html>