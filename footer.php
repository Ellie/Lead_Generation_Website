<!-- section for side links -->
<section>
    <div class="demoicons">
        <div class="sideicons my-2">
            <a href="" class="btn btn-outline-success">Website Demo</a>
        </div>
        <div class="sideicons my-2">
            <a href="" class="btn btn-outline-success">Application Demo</a>
        </div>
        <div class="sideicons my-2">
            <a href="" class="btn btn-outline-success">Admin Panel Demo</a>
        </div>
    </div>
</section>
<!-- end section -->

<!-- footer start -->
<footer>
    <div class="container-fluid footer-top py-3">
        <div class="row">
            <div class="col-lg-2">
                <div class="footerheading">
                    <h4>Policy</h4>
                </div>
                <ul>
                    <li>
                        <a href="">
                            Privacy Policy
                        </a>
                    </li>
                    <li>
                        <a href="">
                            Terms & Conditions
                        </a>
                    </li>
                    <li>
                        <a href="">
                            Refund & Cancellation
                        </a>
                    </li>
                    <li>
                        <a href="">
                            FAQ's
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-2">
                <div class="footerheading">
                    <h4>Quick Links</h4>
                </div>
                <ul>
                    <li>
                        <a href="">
                            Home
                        </a>
                    </li>
                    <li>
                        <a href="">
                            Pricing & Plans
                        </a>
                    </li>
                    <li>
                        <a href="">
                            Go Digital
                        </a>
                    </li>
                    <li>
                        <a href="">
                            Digital Platform
                        </a>
                    </li>
                    <li>
                        <a href="">Steps To Go Digital</a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-2">
                <div class="footerheading">
                    <h4>About</h4>
                </div>
                <ul>
                    <li>
                        <a href="">
                            About Us
                        </a>
                    </li>
                    <li>
                        <a href="">
                            Contact
                        </a>
                    </li>
                    <li>
                        <a href="">
                            Support
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-3">
                <div class="footerheading">
                    <h4>Contact Us</h4>
                </div>
                <ul>
                    <li>
                        <a href=""><span><i class="fas fa-map-marker-alt mr-2"></i>Bijnor, Uttar Pradesh,
                                India</span></a>
                    </li>
                    <li>
                        <a href=""><span><i class="fas fa-phone-alt mr-2"></i>100-2444-444</span></a>
                    </li>
                    <li>
                        <a href=""><span><i class="fas fa-envelope mr-2"></i>info@gmail.com</span></a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-3 subscribe-now text-center">
                <div class="footerheading">
                    <h4>Subscribe Now</h4>
                </div>
                <div class="subscribenewsletter ">
                    <span>Subscribe to our newsletter for updates.</span>
                </div>
                <form action="">
                    <div class="form-group mt-3  mx-4">
                        <label for="exampleFormControlInput1">Email address</label>
                        <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="example@gmail.com" required>
                    </div>
                    <div class="">
                        <input type="submit" class="btn btnsubmit" value="Subscribe">
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="container-fluid footer-bottom py-3">
        <div class="row text-center">
            <div class="col-md-5 my-2">
                <span class="ml-2">
                    &copy;Copyright 2021 Zoyo Ecommerce.com, All Rights Reserved.
                </span>
            </div>

            <div class="col-md-4 my-2 developedby">
                <span>
                    Developed by: Zoyo E-commerce Pvt. Ltd.
                </span>
            </div>

            <div class="col-md-3 my-2 footer-bottom-icon">
                <div class="mr-2">
                    <a href=""><i class="fab fa-facebook"></i></a>
                    <a href=""> <i class="fab fa-twitter"></i></a>
                    <a href=""><i class="fab fa-instagram"></i></a>
                    <a href=""><i class="fab fa-linkedin-in"></i></a>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- footer end -->