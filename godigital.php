<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Go Digital</title>

    <link rel="stylesheet" href="newcss/style.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- fontawesome link -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

</head>

<body>
    <?php include('header.php') ?>
    <!-- navbar end -->

    <!-- top section -->
    <section>
        <div class="container ecommerce-sec mt-4">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-heading">
                        <h2>Why Go Ditigal</h2>
                    </div>
                    <div>
                        <hr>
                    </div>
                    <div>
                        <img class="img-fluid" src="image/Digital-Illistration_IAPI.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- top section end -->

    <!-- section -->
    <section>
        <div class="container ecommerce-sec text-center">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-heading">
                        <h2>Why Should We Enter The Digital
                            World?</h2>
                    </div>
                    <div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>

        <div class="container ecommerce-sec text-center py-3">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-heading">
                        <h3>Global Internet Users</h3>
                    </div>
                </div>
            </div>

            <div class="row mt-3 justify-content-center text-center">
                <div class="col-md-2 my-2">
                    <img src="image/population icon.png" class="img-fluid" alt="">
                    <div class="section-heading mt-2">
                        <h5>Total Population</h5>
                    </div>
                </div>

                <div class="col-md-2 my-2">
                    <img src="image/internet user.png" class="img-fluid" alt="">
                    <div class="section-heading mt-2">
                        <h5>Active Internet Users</h5>
                    </div>
                </div>

                <div class="col-md-2 my-2">
                    <img src="image/social.png" class="img-fluid" alt="">
                    <div class="section-heading mt-2">
                        <h5>Active Social Media accounts</h5>
                    </div>
                </div>

                <div class="col-md-2 my-2">
                    <img src="image/mobile.png" class="img-fluid" alt="">
                    <div class="section-heading mt-2">
                        <h5>Total Mobile Users</h5>
                    </div>
                </div>

                <div class="col-md-2 my-2">
                    <img src="image/mobile-data.png" class="img-fluid" alt="">
                    <div class="section-heading mt-2">
                        <h5>Active Mobile Internet Users</h5>
                    </div>
                </div>
            </div>

            <div class="row py-4">
                <div class="col-md-4 my-2">
                    <div class="card">
                        <img class="img-fluid" src="image/image-1.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5>40% INTERNET
                            </h5>
                            <hr>
                            <p class="card-text">40% of global Internet users have bought products or goods
                                online.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 my-2">
                    <div class="card">
                        <img class="img-fluid" src="image/google-plus.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h4 class="card-title">1.55 BILLION
                            </h4>
                            <hr>
                            <p class="card-text">Google now has 1.55 billion active users.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 my-2">
                    <div class="card">
                        <img class="img-fluid" src="image/facebook.jpg" alt="Card image cap">
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">1.55 BILLION
                            </h4>
                            <hr>
                            <p class="card-text">Facebook now has 1.55 billion active users.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row py-4">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h2>Digital Marketing vs Traditional
                            Marketing
                        </h2>
                    </div>
                    <div>
                        <hr>
                    </div>
                    <div>
                        <img class="img-fluid mt-2" src="image/DigitalvsTraditional.jpg" alt="">
                    </div>
                </div>
            </div>


            <div class="container-fluid py-4">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-heading">
                            <h2>4 Cons of Traditional Marketing</h2>
                        </div>
                        <div>
                            <hr>
                        </div>
                        <div>
                            <img class="img-fluid" src="image/drawbacks.jpg " alt="">
                        </div>
                    </div>
                </div>
            </div>

            <div class="container text-center py-4">
                <div class="section-heading">
                    <h2>What is digital Marketing?</h2>
                </div>
                <div>
                    <hr>
                </div>
                <div class="row text-center mt-2">
                    <div class="col-md-6 my-2">
                        <img class="img-fluid" src="image/what_digital_marketing.jpg" alt="">
                    </div>
                    <div class="col-md-6 my-2">
                        <p class="p-2" style="text-align: justify;">
                            Internet Marketing encompasses all promoting endeavors that utilization an electronic
                            gadget or the Internet. Business influence digital channels, for example, web crawlers,
                            online life, email, and different sites to interface with prospective and planned
                            clients. Digital Marketing is simply marketing.Digital Marketing is doing marketing of
                            products, services, brands or people on the internet using digital devices like
                            laptops,tablets and mobile phones. Internet Marketing is a major component of digital
                            Marketing.
                        </p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 my-2">
                    <div class="section-heading">
                        <h2>6 Pros of Digital Marketing Over Traditional Marketing</h2>
                    </div>
                    <div>
                        <hr>
                    </div>
                    <div class="mt-2">
                        <img class="img-fluid" src="image/benefits_digital_marketing.jpg " alt="">
                    </div>
                </div>
            </div>

            <div class="container mt-4">
                <div class="section-heading">
                    <h2>What is Social Media Marketing.</h2>
                </div>
                <div>
                    <hr>
                </div>
                <div class="row text-center">
                    <div class="col-md-6 mb-2">
                        <img class="img-fluid" src="image/socialmediamarketing01.jpg" alt="">
                    </div>
                    <div class="col-md-6">
                        <p style="text-align: justify;">
                            Social media marketing is a ground-breaking route for organizations of all sizes to
                            arrive at possibilities and clients. Your clients are now associating with brands
                            through internet based life, and in case you're not talking straightforwardly to your
                            group of spectators through social stages like Facebook, Twitter, Instagram, and
                            Pinterest, you're passing up a great opportunity! Incredible promoting via social media
                            can carry wonderful accomplishment to your business. Social media marketing is the use
                            of social media platforms and Digital platform to promote a product or service. Online
                            networking advertising is the utilization of web-based social networking stages to
                            associate with your group of spectators to construct your image...
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- footer start -->

    <?php include('footer.php') ?>


    <!-- footer end -->





    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>