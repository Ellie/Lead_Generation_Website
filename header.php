
    <div class="container-fluid nav-top text-center py-2">
        <div class="row">
            <div class="col-lg-4 nav-top-anchor">
                <div class="my-2">
                    <a href=""><span><i class="fas fa-phone-alt mx-2"></i>100-2444-444</span></a>
                    <a href=""><span><i class="fas fa-envelope mx-2"></i>info@gmail.com</span></a>
                </div>
            </div>
            <div class="col-lg-4 d-none d-md-block">
                <a class="navtopheading" href="#">Zoyo E-commerce Pvt. Ltd.</a>
            </div>
            <div class="col-lg-4 nav-top-icon">
                <div class="my-2">
                    <a href=""><i class="fab fa-facebook"></i></a>
                    <a href=""> <i class="fab fa-twitter"></i></a>
                    <a href=""><i class="fab fa-instagram"></i></a>
                    <a href=""><i class="fab fa-linkedin-in"></i></a>
                </div>
            </div>
        </div>
    </div>

    <nav class="navbar navbar-expand-lg navbar-top py-4 sticky-top">
        <a class="navbar-brand" href="#">Zoyo E-commerce</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-bars"></i></button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav ml-auto mx-2">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="pricingplan.php">Pricing & Plan</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="godigital.php">Go Digital</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="steptogodigital.php">Steps to Go Digital</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="digitalplatform.php">Digital Platform</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="faq.php">FAQ's</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="blog.php">Blog</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="contact.php">Contact</a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- navbar end -->