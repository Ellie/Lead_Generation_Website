<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Web Demo</title>

    <link rel="stylesheet" href="newcss/style.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- fontawesome link -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
</head>

<body>
    <?php include('header.php') ?>

    <!-- navbar end -->

    <!-- top section -->
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block " src="image/dm.jpg" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block " src="image/Custom-Software-Development.jpg" alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block " src="image/dm.jpg" alt="Third slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <!-- top section end -->


    <!-- section -->
    <section>
        <div class="container-fluid ecommerce-sec">
            <div class="row justify-content-center">
                <div class="col-md-6 text-center">
                    <div class="section-heading">
                        <h2 style="margin: 25% 25% 25% 25%;">Get Your Website Online In 3 Easy Steps</h2>
                    </div>
                </div>

                <div class="col-md-6 step ">
                    <div class="mb-3">
                        <img src="image/step1.png" class="img-fluid" alt="">
                        <span class="stepheading">Step 1: Select a Plan.</span>
                    </div>
                    <div>

                    </div>
                    <div class="mt-3">
                        <span>As per your Business, Select eCommerce Plan.
                        </span>
                    </div>
                    <div class="mt-4">
                        <img src="image/step1.png" class="img-fluid" alt="">
                        <span class="stepheading"> Step 2: Select Your Business Domain Name.</span>
                        <div class="mt-3">
                            <span>As per your Business Name, Select Business Domain.
                            </span>
                        </div>
                    </div>
                    <div class="mt-4">
                        <img src="image/step1.png" class="img-fluid" alt="">
                        <span class="stepheading">Step 3: Place an Order and Get we services.</span>
                        <div class="mt-3">
                            <span>Checkout & As Domain Transfered to Your Account, Your eCommerce Website will be Live.
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- section end -->



    <!-- section3 -->
    <section class="ecommerce-section">
        <div class="container mt-4 ecommerce-sec">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-heading">
                        <h2>15 Key of eCommerce Features</h2>
                    </div>
                    <div>
                        <hr>
                    </div>
                </div>
            </div>

            <div class="row my-2">
                <div class="col-md-4">
                    <div class="icon-box my-2">
                        <a href="">
                            <div class="card text-center align-items-center">
                                <img class="img-fluid" style="border-radius: 10%" src="image/domain (2).jfif"
                                    alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">Domain </h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="icon-box my-2">
                        <a href="">
                            <div class="card text-center align-items-center">
                                <img class="img-fluid" style="border-radius: 10%" src="image/hosting (2).jfif"
                                    alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">Hosting </h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="icon-box my-2">
                        <a href="">
                            <div class="card text-center align-items-center">
                                <img class="img-fluid" style="border-radius: 10%" src="image/ios (2).jpg"
                                    alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">IOS </h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="row my-2">
                <div class="col-md-4">
                    <div class="icon-box my-2">
                        <a href="">
                            <div class="card text-center align-items-center">
                                <img class="img-fluid" style="border-radius: 10%" src="image/Ecommerce (1).png"
                                    alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">Ecommercce </h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="icon-box my-2">
                        <a href="">
                            <div class="card text-center align-items-center">
                                <img class="img-fluid" style="border-radius: 10%" src="image/vendor (1).jpg"
                                    alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">Vendor </h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="icon-box my-2">
                        <a href="">
                            <div class="card text-center align-items-center">
                                <img class="img-fluid" style="border-radius: 10%"
                                    src="image/customer_management (1).jfif" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">Customer Managemaent </h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>


            <div class="row my-2">
                <div class="col-md-4">
                    <div class="icon-box my-2">
                        <a href="">
                            <div class="card text-center align-items-center">
                                <img class="img-fluid" style="border-radius: 10%" src="image/order_management (1).png"
                                    alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">Order Management</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="icon-box my-2">
                        <a href="">
                            <div class="card text-center align-items-center">
                                <img class="img-fluid" style="border-radius: 10%" src="image/sms (1).jpg"
                                    alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">SMS </h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="icon-box my-2">
                        <a href="">
                            <div class="card text-center align-items-center">
                                <img class="img-fluid" style="border-radius: 10%" src="image/payment (2).jfif"
                                    alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">Payment </h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="row my-2">
                <div class="col-md-4">
                    <div class="icon-box my-2">
                        <a href="">
                            <div class="card text-center align-items-center">
                                <img class="img-fluid" style="border-radius: 10%" src="image/product_management (1).png"
                                    alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">Product Management </h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="icon-box my-2">
                        <a href="">
                            <div class="card text-center align-items-center">
                                <img class="img-fluid" style="border-radius: 10%" src="image/shiping (1).jfif"
                                    alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">Shiping </h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="icon-box my-2">
                        <a href="">
                            <div class="card text-center align-items-center">
                                <img class="img-fluid" style="border-radius: 10%" src="image/Email (1).jpg"
                                    alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">Email </h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="row my-2">
                <div class="col-md-4">
                    <div class="icon-box my-2">
                        <a href="">
                            <div class="card text-center align-items-center">
                                <img class="img-fluid" style="border-radius: 10%" src="image/vendor_management (2).jfif"
                                    alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">Vendor Management </h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="icon-box my-2">
                        <a href="">
                            <div class="card text-center align-items-center">
                                <img class="img-fluid" style="border-radius: 10%" src="image/android (1).png"
                                    alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">Android </h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="icon-box my-2">
                        <a href="">
                            <div class="card text-center align-items-center">
                                <img class="img-fluid" style="border-radius: 10%"
                                    src="image/product_review_management (1).jfif" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="card-title">Product Review Management</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- section3 end -->

    <!-- top section -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center ecommerce-sec">
                    <div class="section-heading">
                        <h2>eCommerce Website & Application demo store</h2>
                    </div>
                    <div>
                        <hr>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="text-center mb-3">
                        <h4>Be Sure after Check Demo Website & Application</h4>
                    </div>
                    <div class="webdemobtn">
                        <a href="" class="btn btnsubmit mx-4 my-3">Website Demo</a>
                        <a href="" class="btn btnsubmit mx-4 my-3">Application Demo</a>
                        <a href="" class="btn btnsubmit mx-4 my-3">Admin Panel Demo</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- section end -->


    <section>
        <div class="container py-3">
            <div class="row">
                <div class="col-md-12 text-center ecommerce-sec">
                    <div class="section-heading">
                        <h2>Why Digital Platform Is Must For
                            Your Business?</h2>
                    </div>
                    <div>
                        <hr>
                    </div>
                </div>
            </div>

            <div class="row p-4 justify-content-center">
                <div class="col-md-3 text-center">
                    <img src="image/character.svg" class="img-fluid" alt="">
                </div>
                <div class="col-md-4">
                    <div class="my-4 ml-4">
                        <ul>
                            <li>Get New Customers.</li>
                            <li>Boost Business Online.</li>
                            <li>Extend My Business Globally.</li>
                            <li>Everybody Searches Online Digital Platform.</li>
                            <li>Simple To Update A Website And Keep People Educated.</li>
                            <li>My Challenger Has A Website.</li>
                            <li>Advantageous For Customers.</li>
                            <li>Eco-accommodating and Not Print Brochures.</li>
                            <li>Virtual Sales Force.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container text-center">

            <div class="row">
                <div class="col-md-12 text-center ecommerce-sec">
          <div class="section-heading">
                        <h2>We Are Pleased to Offer Zoyo Ecommerce Services</h2>
                    </div>
                    <div>
                        <hr>
                    </div>
                    <div class="section-heading">
                        <h2 class="mt-4">What We Offer</h2>
                    </div>
                </div>
            </div>

            <div class="mt-4">
                <p>
                    Zoyo Ecommerce provides services like Website Development, eCommerce Website Development , Android
                    Application Development, Graphic Design, Motion Graphics and Digital Marketing services. AJ Infosoft
                    are intended to help developing organizations by offering completely oversaw innovation arrangements
                    at a reasonable expense. We will likewise deal with all software and platform.
                </p>
            </div>

            <div class="row mt-4">
                <div class="col-md-4 my-2">
                    <div class="card align-items-center">
                        <img class="img-fluid w-25 mt-2" src="image/web-devlopment.png" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">
                                Web Development
                            </h5>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 my-2">
                    <div class="card align-items-center">
                        <img class="img-fluid w-25 mt-2" src="image/app-devlopmrnt.png" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">
                                Application Development
                            </h5>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 my-2">
                    <div class="card align-items-center">
                        <img class="img-fluid w-25 mt-2" src="image/digial-marketing.png" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">
                                Digital Marketing
                            </h5>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 my-2">
                    <div class="card align-items-center">
                        <img class="img-fluid w-25 mt-2" src="image/graphic.png" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">
                                Graphic Design
                            </h5>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 my-2">
                    <div class="card align-items-center">
                        <img class="img-fluid w-25 mt-2" src="image/bussiness-lead.png" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">
                                Business Lead Generation
                            </h5>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 my-2">
                    <div class="card align-items-center">
                        <img class="img-fluid w-25 mt-2" src="image/blue-tick.png" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">
                                Blue Tick Verification
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container ecommerce-sec">
            <div class="row">
                <div class="col-md-6 py-4">
                    <img src="image/e-commerce-application-development-service-500x500.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-md-6 py-4">
                    <div class="section-heading text-center">
                        <h3>Why zoyo eCommerce?</h3>
                    </div>
                    <div>
                        <hr>
                    </div>
                    <p class="">
                        we provide best services to our clients by keeping all their needs in our mind. We are always
                        available to help our clients whenever needed. We keep ourselves up-to-date with the upcoming
                        trend to give you latest features added in our product.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6  py-4">
                    <div class="section-heading text-center">
                        <h3>What We Do?</h3>
                    </div>
                    <div>
                        <hr>
                    </div>
                    <p class="">
                        We create custom, functional websites, APP and focused on letting our clients business online.
                    </p>
                    <!-- <p>
                        An eCommerce Solution (or eCommerce Solution) alludes to programming like Storefront, by Awesome
                        Commerce, LLC. Customer facing facade a Software arrangement utilized by organizations and
                        traders that need to sell items on the web.
                    </p> -->
                </div>
                <div class="col-md-6 py-4">
                    <img src="image/e-commerce-application-development-service-500x500.jpg" class="img-fluid" alt="">
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 py-4">
                    <img src="image/e-commerce-application-development-service-500x500.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-md-6 py-4">
                    <div class="section-heading text-center">
                        <h3>Advantage of choosing Zoyo eCommerce.</h3>
                    </div>
                    <div>
                        <hr>
                    </div>
                    <p>
                        They improve the way toward purchasing and selling . They reduce the land hole so you can
                        arrive at more clients and sell items all day, every day, since the Internet is continually
                        dynamic; it implies that your business never needs to close. They lower operational costs,
                        publicizing and promoting and faculty costs. They wipe out the need to do physical set-ups of
                        organizations. The arrangements make it simple to begin and deal with a business. Provide
                        examination shopping – they give customers alternatives to contrast and which means your
                        organization will have an edge on the off chance that they have quality items at moderate costs.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <!-- section end -->


    <!-- footer start -->

    <?php include('footer.php') ?>


    <!-- footer end -->





    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</body>

</html>