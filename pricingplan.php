<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Pricing and Plan for zoyo</title>

    <link rel="stylesheet" href="newcss/style.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- fontawesome link -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
</head>

<body class="pricing-plan-body">
    <?php include('header.php') ?>

    <!-- navbar end -->

    <!-- top section -->
    <section>
        <div class="container ecommerce-sec">
            <div class="row ecommerce-section mt-5">
                <div class="col-md-12 text-center">
                    <div class="section-heading">
                        <h2>E-commerce Package</h2>
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </section>
    <!-- top section end -->

    <!-- section -->
    <section>
        <div class="container ecommerce-sec">
            <div class="row plan-pricing">
                <div class="col-md-3 my-2">
                    <div class="ecommerce-section">
                        <div class="section-heading text-center">
                            <h3>Startup Plan</h3>
                        </div>
                        <div class="rate text-center my-3">
                            <span class="mr-4" style="font-size: 25px;">&#8377;5,999</span><span class="rightrate" style="font-size: 22px;"><s>&#8377;11,999</s></span>
                        </div>
                        <div class="my-3">
                            <div class="section-heading">
                                <h5 class="my-2">FEATURES</h5>
                            </div>
                            <div>
                                <ul>
                                    <li>
                                        <a href="">
                                            <span>Domain | Hosting</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <span>SSL Certificate (HTTPS)</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <span><strong>Ecommerce Website</strong></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <span>Deal of The Day</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <span>Coupon Management</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <span>SMS Notification</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <span>Product wise Shipping</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <span>Weight wise Shipping</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <span>Order Base Shipping</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <span>Store Pickup</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <span>PayOn Pickup</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <span><strong>Payment Gateway(RazorPay)</strong></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <span><strong>(Credit Card | Debit Card | Net Banking | BHIM)</strong></span>
                                        </a>
                                    </li>
                                </ul>
                                <div class="py-2 text-center">
                                    <a href="" class="btn btn-primary">
                                        <span>Active this Plan</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 my-2">
                    <div class="ecommerce-section">
                        <div class="section-heading text-center">
                            <h3>Business Plan</h3>
                        </div>
                        <div class="rate text-center my-3">
                            <span class="mr-4" style="font-size: 25px;">&#8377;9,999</span><span class="rightrate" style="font-size: 22px;"><s>&#8377;19,999</s></span>
                        </div>
                        <div class="my-3">
                            <div class="section-heading">
                                <h5>FEATURES</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 my-2">
                    <div class="ecommerce-section">
                        <div class="section-heading text-center">
                            <h3>Growth Plan</h3>
                        </div>
                        <div class="rate text-center my-3">
                            <span class="mr-4" style="font-size: 25px;">&#8377;14,999</span><span class="rightrate" style="font-size: 22px;"><s>&#8377;29,999</s></span>
                        </div>
                        <div class="my-3">
                            <div class="section-heading">
                                <h5>FEATURES</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 my-2">
                    <div class="ecommerce-section">
                        <div class="section-heading text-center">
                            <h3>Ultimate Plan</h3>
                        </div>
                        <div class="rate text-center my-3">
                            <span class="mr-4" style="font-size: 25px;">&#8377;19,999</span><span class="rightrate" style="font-size: 22px;"><s>&#8377;39,999</s></span>
                        </div>
                        <div class="my-3">
                            <div class="section-heading">
                                <h5>FEATURES</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- section end -->

    <!-- footer start -->
    <?php include('footer.php') ?>
    <!-- footer end -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>