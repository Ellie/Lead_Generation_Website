<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Web Demo</title>

  <link rel="stylesheet" href="newcss/style.css">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  <!-- fontawesome link -->
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
    integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

</head>

<body>
  <?php include('header.php')?>

  <!-- navbar end -->

  <!-- top section -->
  <div class="container py-4">
    <div>
      <img src="image/3-SEO-strategie.png " class="img-fluid" alt="">
    </div>
  </div>
  <section>
    <div class="col-md-12 text-center ecommerce-sec">
      <div class="section-heading">
        <h2>Steps To Grow Your Business.
        </h2>
      </div>
      <div>
        <hr>
      </div>
    </div>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-2 ">
          <div class="card">
            <img class="card-img-top" src="image/customer.jpg" alt="Card image cap">
            <div class="card-body">
              <h3 class="text-center">1</h3>
              <p class="card-text text-center">CUSTOMER</p>
            </div>
          </div>
        </div>
        <div class="col-md-2">
          <div class="card">
            <img class="card-img-top" src="image/steptodigital.jpg" alt="Card image cap">
            <div class="card-body">
              <h3 class="text-center">2</h3>
              <p class="card-text text-center">STEP GO DIGITAL</p>
            </div>
          </div>
        </div>
        <div class="col-md-2">
          <div class="card">
            <img class="card-img-top" src="image/digital platform.jpg" alt="Card image cap">
            <div class="card-body">
              <h3 class="text-center">3</h3>
              <p class="card-text text-center">GET DIGITAL PLATFORM</p>
            </div>
          </div>
        </div>
        <div class="col-md-2">
          <div class="card">
            <img class="card-img-top" src="image/getorder.jpg" alt="Card image cap">
            <div class="card-body">
              <h3 class="text-center">4</h3>
              <p class="card-text text-center">GET ORDER & INCOME</p>
            </div>
          </div>
        </div>
        <div class="col-md-2">
          <div class="card">
            <img class="card-img-top" src="image/grow.jpg" alt="Card image cap">
            <div class="card-body">
              <h3 class="text-center">5</h3>
              <p class="card-text text-center">GROW BUSINESS</p>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>
  <section>
    <div class="container justify-content-center">
      <div class="row justify-content-between">
        <div class="col-md-6">
          <div class="card text-white bg-info mb-3 p-2">
            <h3 class="text-center">VIRTUAL SALES FORCE
            </h3>
            <hr>
            <span class="text-center">Virtual Salesman ToGenerate Leads & Sales Revenue.
            </span>
            <hr>
            <h3 class="text-center">WORK 24/7
            </h3>
            <hr>
            <span class="text-center">Work 24 Hours A Day, 7 Days A Week & 365 Days A Year. No Public Holidays.
            </span>
            <hr>
            <h3 class="text-center">SAVE MONEY
            </h3>
            <hr>
            <span class="text-center">Cheap Space, Low-cost Maintenance & It Won’t Ask You For A Salary Or A Bonus.
            </span>
            <hr>
            <h3 class="text-center">GROW YOUR AUDIENCE
            </h3>
            <hr>
            <span class="text-center">Cross international borders and serve multiple Customers at once. Encourage
              referral traffic.
            </span>
          </div>

        </div>
        <div class="col-md-4 text-center">
          <img src="image/character (1).svg" alt="">
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="container py-4 " style="background-color:  rgb(241, 241, 241);">
      <div class="row">
        <div class="col-md-12 text-center ecommerce-sec">
          <div class="section-heading">
            <h2>5 Secrets to Get More Visibility from Various Platform
            </h2>
          </div>
          <div>
            <hr>
          </div>
        </div>
      </div>
    </div>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <p style="text-align: justify;">
            The point of any business is to make a benefit yet there's no benefit if individuals don't think about your
            business. It's tied in with expanding the perceivability of your organization. As a bustling proprietor, you
            may
            rapidly wind up in a period and spending smash with your advertising. Thusly, you lose chances to develop
            your
            business.
          </p>
          <h4>I suggest 5 speedy thoughts you can use to build the perceivability of your small business.
          </h4>
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="container py-4 ">
      <div class="row">
        <div class="col-md-12 text-center ecommerce-sec">
          <div class="section-heading">
            <h2>Build Your Own Website
            </h2>
          </div>
          <div>
            <hr>
          </div>
        </div>
      </div>
    </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <img src="image/step_one_to_go_digital (1).jpg" class="img-fluid" alt="">
        </div>
        <div class="col-md-6" style="text-align: justify;">
          <p>
            A site is the online personality of your business. In an age where the world is moving towards computerized,
            it's stunning to perceive what number of organizations still overlook having an online nearness. Your
            clients
            anticipate that you should have a computerized proximity where they can discover more data about you and
            your
            items or administrations.
          </p>
          <p>
            You needn't bother with broad coding or specialized abilities to make your very own site. These days, you
            can
            set up your site in minutes utilizing apparatuses like WordPress or Wix which offer free lifetime account
            and
            numerous valuable formats which can be redone as indicated by your business necessities.
          </p>
        </div>

      </div>
    </div>

  </section>

  <section>

    <div class="col-md-12 text-center ecommerce-sec">
      <div class="section-heading">
        <h2>Invest in Networking
        </h2>
      </div>
      <div>
        <hr>
      </div>
    </div>
    </div>
    <div class="container py-4" style="background-color:  rgb(241, 241, 241);">
      <div class="row">

        <div class="col-md-6" style="text-align: justify;">
          <p>
            A site is the online personality of your business. In an age where the world is moving towards computerized,
            it's stunning to perceive what number of organizations still overlook having an online nearness. Your
            clients
            anticipate that you should have a computerized proximity where they can discover more data about you and
            your
            items or administrations.
          </p>
          <p>
            You needn't bother with broad coding or specialized abilities to make your very own site. These days, you
            can
            set up your site in minutes utilizing apparatuses like WordPress or Wix which offer free lifetime account
            and
            numerous valuable formats which can be redone as indicated by your business necessities.
          </p>
        </div>
        <div class="col-md-6 ">
          <img src="image/step_two_to_go_digital.jpg" class="img-fluid" alt="">
        </div>
  </section>
  <section>
    <div class="col-md-12 text-center ecommerce-sec">
      <div class="section-heading">
        <h2>Do the Right Social Media Marketing
        </h2>
      </div>
      <div>
        <hr>
      </div>
    </div>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <img src="image/step_three_to_go_digital.jpg" class="img-fluid" alt="">
        </div>
        <div class="col-md-6" style="text-align: justify;">
          <p>
            Web-based social networking showcasing is a ground-breaking route for organizations of all sizes to arrive
            at prospects and clients. Your clients are as of now interfacing with brands through web based life, and in
            case you're not talking straightforwardly to your group of spectators through social stages like Facebook,
            Twitter, Instagram, and Pinterest, you're passing up a major opportunity! Extraordinary showcasing via
            web-based networking media can carry momentous accomplishment to your business, making committed brand
            advocates.
          </p>
        </div>

      </div>
    </div>
  </section>

  <section>

    <div class="col-md-12 text-center ecommerce-sec">
      <div class="section-heading">
        <h2>Do Content Marketing: Always Audience First
        </h2>
      </div>
      <div>
        <hr>
      </div>
    </div>
    </div>
    <div class="container py-4" style="background-color:  rgb(241, 241, 241);">
      <div class="row">

        <div class="col-md-6" style="text-align: justify;">
          <p>
            As a business owner , you need to advance and market your items or administrations without surpassing your
            spending limit. Substance advertising encourages you connect with new leads and new target markets without
            being excessively costly.
          </p>
          <p>
            Substance promoting isn't just about making an intrigue and building a brand for your business yet it
            centers around offering some incentive to your clients and giving something important to the group of
            spectators to construct trust. Research done by Demand Metric found that substance showcasing isn't just
            more affordable than outbound promoting yet additionally produces multiple times more leads.
          </p>
        </div>
        <div class="col-md-6 ">
          <img src="image/step_four_to_go_digital.jpg" class="img-fluid" alt="">
        </div>
  </section>

  <section>
    <div class="col-md-12 text-center ecommerce-sec">
      <div class="section-heading">
        <h2>Evaluate and Repeat

        </h2>
      </div>
      <div>
        <hr>
      </div>
    </div>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <img src="image/step_five_to_go_digital.jpg" class="img-fluid" alt="">
        </div>
        <div class="col-md-6" style="text-align: justify;">
          <p>
            Such a large number of entrepreneurs don't center their endeavors and basically take a stab at all that they
            can to develop their business. A superior business procedure is to gauge your endeavors with the correct
            measurements and rehash the activities which give the best outcomes with the least exertion.
          </p>
          <p>
            Monitor your online social media networking showcasing and substance advertising endeavors.
            Break down which channels and which backlinks pulled in the most clients for your business.
            Check the quantity of arrangements made on your timetable following 7 days of systems administration
            occasions.
          </p>
        </div>

      </div>
    </div>
  </section>




  <!-- footer start -->

  <?php include('footer.php')?>
  <!-- footer end -->





  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>
</body>

</html>